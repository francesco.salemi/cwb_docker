#!/bin/bash -e


#export LSCSOFT_ROOTDIR="${HOME}/SOFT/LAL/lalsuite_lal-v6.16.1_jessie" # Place where the built lal goes, Make sure folder exists before
#export LSCSOFT_ROOTDIR="${HOME}/SOFT/LAL/lalsuite-v6.67" # Place where the built lal goes, Make sure folder exists before
export LSCSOFT_ROOTDIR="${HOME}/SOFT/LAL/LAL_VERSION" # Place where the built lal goes, Make sure folder exists before
#export LSCSOFT_ROOTDIR="/data/SOFT/LAL/lalsuite_lal-phenomHMSK" # Place where the built lal goes, Make sure folder exists before
#export LSCSOFT_ROOTDIR="/data/SOFT/LAL/lalsuite_master160119" # Place where the built lal goes, Make sure folder exists before
export LSCSOFT_SRCDIR="${HOME}/git"
#export LSCSOFT_SRCDIR="/data/SOFT/LAL"

OLD_WD=$PWD
cd ${LSCSOFT_SRCDIR}/lalsuite

# reset repo to clean state
git clean -f -d -x

# wipe old build
rm -rf ${LSCSOFT_ROOTDIR}/*

# do build

./00boot

CONF_OPTS="--prefix=${LSCSOFT_ROOTDIR} \
    --enable-swig-python \
    --disable-lalstochastic \
    --disable-lalpulsar \
    --disable-lalinference \
    --disable-gcc-flags " # \ 
#    --disable-debug"


./configure ${CONF_OPTS} > >(tee /tmp/stdout.log) 2> >(tee /tmp/stderr.log >&2)


make && make install

# this is necessary to get pylal libs linked properly
source ${LSCSOFT_ROOTDIR}/etc/lal-user-env.sh
source ${LSCSOFT_ROOTDIR}/etc/lalapps-user-env.sh

#export GLUE_LOCATION=$LSCSOFT_ROOTDIR
#cd ${LSCSOFT_SRCDIR}/lalsuite/glue
#python setup.py install --prefix=${GLUE_LOCATION}

#export PYLAL_LOCATION=$LSCSOFT_ROOTDIR
#cd ${LSCSOFT_SRCDIR}/lalsuite/pylal
#python setup.py install --prefix=${PYLAL_LOCATION}

cd ${OLD_WD}

