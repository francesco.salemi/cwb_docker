#!/bin/bash

# In order to activate the conda environment
# in the next step, we need the definitions
# that 'conda init' has placed into this file
source /etc/profile.d/conda.sh

# The environment variable below should
# have been defined by 
conda activate $CWB_CONDA_ENVIRONMENT

export BASH_ENV="/etc/profile.d/conda.sh"

exec $@
