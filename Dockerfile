FROM igwn/base:el8

USER root
SHELL ["/bin/bash", "-c"]

COPY auxfiles/conda.repo /etc/yum.repos.d/conda.repo

RUN rpm --import https://repo.anaconda.com/pkgs/misc/gpgkeys/anaconda.asc
RUN dnf -y install conda which zsh git git-lfs glibc-locale-source glibc-langpack-en gwdatafind ligo-proxy-utils vim-minimal vim-enhanced && dnf clean all

ARG CONDAPATH=/opt/conda
ARG CONDA=${CONDAPATH}/bin/conda
ARG CONDA_ENV_NAME="cwb_conda"

COPY auxfiles/cwb_conda_devel.env /etc/conda/cwb_conda_devel.env

# Setup conda environment, and initialize shells
RUN ${CONDA} create -y -n ${CONDA_ENV_NAME} --file /etc/conda/cwb_conda_devel.env && \
    ${CONDA} clean --all --force-pkgs-dirs --yes && \
    mkdir -p ${CONDAPATH}/pkgs/ && \
    touch ${CONDAPATH}/pkgs/urls.txt 

# Add localedef
RUN localedef -i en_US -f UTF-8 en_US.UTF-8 

RUN ${CONDA} init --no-user --system --all 

# Sadly, the above seems to do nothing for the system installed zsh
# We also modify zshrc to start everyone in the conda environment
COPY auxfiles/zshenv /tmp/zshenv
COPY auxfiles/zshrc /tmp/zshrc
RUN cat /tmp/zshenv >> /etc/zshenv && rm -f /tmp/zshenv && \
    cat /tmp/zshrc >> /etc/zshrc && rm -f /tmp/zshrc

ARG BASH_ENV=/etc/bashrc

## Clone the repo, activate the conda environment, and build & install into the environment

ARG DOCKER_BLD_DIR=/bld/cwb

# The steps below will setup the conda environment for building, and it will
# remain that way when activated at a shell inside a container. We do this in
# part because users may start this container interactively to build plugins

RUN conda env config vars set CONDA_BUILD=1 -n ${CONDA_ENV_NAME} && \
    conda activate ${CONDA_ENV_NAME} && \
    conda env config vars set PREFIX=${CONDA_PREFIX} && \
    conda activate ${CONDA_ENV_NAME} && \
    rm -rf ${DOCKER_BLD_DIR} && mkdir -p ${DOCKER_BLD_DIR} && cd ${DOCKER_BLD_DIR} && \
#    curl -SL "https://gitlab.com/gwburst/public/library/-/archive/cWB-6.4.2/library-cWB-6.4.2.tar.gz" -o library-cWB-6.4.2.tar.gz &&  tar zxf library-cWB-6.4.2.tar.gz && cd library-cWB-6.4.2 && \
    git clone -b 'cWB-6.4.2.99.test_conda' --single-branch https://gitlab.com/gwburst/public/library.git && cd library && \
    mkdir -p build && cd build && \
    cmake ${CMAKE_ARGS} .. && \
    cmake --build . && \
    cmake --install . && \
    cp ${CONDA_PREFIX}/etc/cwb/cwb-activate.sh ${CONDA_PREFIX}/etc/conda/activate.d/activate-cwb.sh && \
    cp ${CONDA_PREFIX}/etc/cwb/cwb-deactivate.sh ${CONDA_PREFIX}/etc/conda/deactivate.d/deactivate-cwb.sh && \
    rm -rf ${DOCKER_BLD_DIR}

# Setup cWB config files

ARG DOCKER_CWB_CONFIG_DIR=/cwb/config

RUN rm -rf ${DOCKER_CWB_CONFIG_DIR} && mkdir -p ${DOCKER_CWB_CONFIG_DIR} && \
    rm -rf ${DOCKER_BLD_DIR} && mkdir -p ${DOCKER_BLD_DIR} && cd ${DOCKER_BLD_DIR} && \
    git clone -b 'public' --single-branch --depth 1 https://gitlab.com/gwburst/public/config_o3.git config && \
    cd config && \
    git lfs pull -I "XTALKS/wdmXTalk/*" && \
    conda activate ${CONDA_ENV_NAME} && \
    make DATA=GWOSC O3=O3b && \
    mv -t ${DOCKER_CWB_CONFIG_DIR} ${DOCKER_BLD_DIR}/config/* && \
    cd / && rm -rf ${DOCKER_BLD_DIR} && \
    sed 's/Rint\.Logon:.*/Rint\.Logon:              \$CWB_ROOTLOGON_FILE/g' -i ${CONDA_PREFIX}/etc/system.rootrc

ENV CWB_CONFIG ${DOCKER_CWB_CONFIG_DIR}
ENV CWB_ANALYSIS="2G"
ENV HOME_WAT_FILTERS="${CWB_CONFIG}/XTALKS"

CMD ["/bin/zsh"]
