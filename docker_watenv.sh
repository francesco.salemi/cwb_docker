#  -----------------------------------------------------------------
#  Plot ascii logo
#  -----------------------------------------------------------------

if hash toilet 2>/dev/null; then 
	toilet -f bigmono12 --filter border  --metal \   cWB \    
else
	echo -e " \n 
BEWARE: cWB ascii logo has not been printed due to a missing command on this system: \n
if possible, please install the \"toilet\" command.				"
fi

#  -----------------------------------------------------------------
#  WAT env for cWB DOCKER
#  -----------------------------------------------------------------

  if [[ -z $LD_LIBRARY_PATH  ]]; then
    export LD_LIBRARY_PATH=""
  fi

  export SITE_CLUSTER="DOCKER"
#  export BATCH_SYSTEM="CONDOR"

  
  unset _USE_ICC
  #export _USE_ICC=1
  #unset _USE_CPP11        
  export  _USE_CPP11=1        
  
  #unset _USE_ROOT6        
  export _USE_ROOT6=1        
  

#  -----------------------------------------------------------------
#  this section is mandatory to compile the WAT libraries
#  -----------------------------------------------------------------

  
 # export HOME_LIBS="${HOME}/SOFT" 
  export HOME_LIBS="/usr/lib/x86_64-linux-gnu/" 
  
  #export ROOT_VERSION="root-v5-34-25_jessie_ICC2015"
  #export ROOT_VERSION="root-v6-13-02_jessie_ICC18"
  #export ROOT_VERSION="root_v6-14-00"
  #export ROOT_VERSION="root_v6-14-00_gcc8.2.0-7"
  #export ROOT_VERSION="root-v6-22-06"
  #export ROOT_VERSION="root"
  #export ROOT_VERSION="root-v5-34-25_icc2018"
 # export ROOTSYS="${HOME_LIBS}/ROOT/${ROOT_VERSION}"
  export ROOTSYS="/usr/local"
  #
  #export ROOTSYS="/home/waveburst/SOFT/ROOT/${ROOT_VERSION}"

  
  #export HOME_FRLIB="${HOME_LIBS}/FRAMELIB/libframe-8.30_root-v5-34-25_icc"
  #export HOME_FRLIB="${HOME_LIBS}/FRAMELIB/libframe-8.30_root-6.13.02"
#  export HOME_FRLIB="${HOME_LIBS}/FRAMELIB/libframe-8.30_root-6.14.00_icc"
  #export HOME_FRLIB="${HOME_LIBS}/FRAMELIB/libframe-8.30_root6.06.02"
  export HOME_FRLIB="${HOME_LIBS}/FRAMELIB/framelib-v8r41p1"

  
  
  export _USE_HEALPIX=1
  #export HOME_HEALPIX="${HOME_LIBS}/HEALPix/Healpix_3.31"
  export HOME_HEALPIX="${HOME_LIBS}/HEALPix/syspkg"
  #export HOME_CFITSIO="${HOME_LIBS}/CFITSIO/cfitsio-3.34"
  export HOME_CFITSIO="${HOME_LIBS}/CFITSIO/syspkg"

  
  
  export _USE_LAL=1
  #unset _USE_LAL
  #export HOME_LAL="${HOME_LIBS}/LAL/lalsuite_v6.79"
  export HOME_LAL="${HOME_LIBS}/LAL/syspkg"
  #export HOME_LAL="${HOME_LIBS}/LAL/LAL_VERSION"
  export LAL_INC="${HOME_LAL}/include"
  export LALINSPINJ_EXEC="${HOME_LAL}/bin/lalapps_inspinj"

  
  
  export _USE_EBBH=1
  #export HOME_CVODE="${HOME_LIBS}/CVODE/cvode-2.7.0/dist"
  export HOME_CVODE="${HOME_LIBS}/CVODE/syspkg"
#  -----------------------------------------------------------------
#  this section is specific for the CWB pipeline 
#  -----------------------------------------------------------------

  
  export CWB_ANALYSIS="2G"
  
  export CWB_CONFIG="/tmp/config"
  #export CWB_CONFIG="/work/salemi/git/cWB/config"
  #export CWB_CONFIG="/home/waveburst/CONFIGS/cWB-config-master.2.8"


  export HOME_WAT_FILTERS="${CWB_CONFIG}/XTALKS"

  
  
  #export LD_LIBRARY_PATH=${HOME_LIBS}/SYSLIBS:$LD_LIBRARY_PATH

#  -----------------------------------------------------------------
#  DO NOT MODIFY !!!
#
#  1) In this section the HOME_WAT env is automatically initialized 
#  2) Init Default cWB library 
#  3) Init Default cWB config  
#  -----------------------------------------------------------------

  
   MYSHELL=`readlink /proc/$$/exe`
  if [[  "$MYSHELL" =~ "tcsh"  ]]; then
    echo "\nEntering in TCSH section..."
     CWB_CALLED=($_)
    if [[=$#CWB_CALLED = 2 ]]; then # alias
      set CWB_CALLED=`alias=$CWB_CALLED`
    fi
    if [[  "$CWB_CALLED" != ""  ]]; then     
       WATENV_SCRIPT=`readlink -f $CWB_CALLED[2]`
    else                                
      echo "\nError: script must be executed with source command"
      return 0 1
    fi
     script_dir=`dirname $WATENV_SCRIPT`
  fi
  if [[  "$MYSHELL" =~ "bash"  ]]; then
    echo ""
    echo "Entering in BASH section..."
    script_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd ) #_SKIP_CSH2SH_
  fi
  if [[  "$MYSHELL" =~ "zsh"  ]]; then
    echo "\nEntering in ZSH section..."
    script_dir=$( cd "$( dirname "${(%):-%N}" )" && pwd )        #_SKIP_CSH2SH_
  fi
  export HOME_WAT=$script_dir
  export HOME_WAT_INSTALL="/usr/local/etc/cwb"
  echo ""
  echo "------------------------------------------------------------------------"
  echo " -> HOME_WAT = $HOME_WAT"
  echo "------------------------------------------------------------------------"
  echo 


# source $HOME_WAT/tools/config.sh     
#  source $CWB_CONFIG/setup.sh          
       

  
  export CWB_HTML_INDEX="${CWB_MACROS}/html_templates/html_index_template_modern.txt"
  
  source /usr/local/etc/cwb/cwb-activate.sh
  source $CWB_SCRIPTS/cwb_watenv.sh
